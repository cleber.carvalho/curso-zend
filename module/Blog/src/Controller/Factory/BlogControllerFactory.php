<?php
/**
 * Created by PhpStorm.
 * User: Cleber Carvalho
 * Date: 26/06/2017
 * Time: 21:15
 */

namespace Blog\Controller\Factory;

use Blog\Controller\BlogController;
use Blog\Model\PostTable;
use Interop\Container\ContainerInterface;

class BlogControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        return new BlogController(
            $container->get(PostTable::class)
        );
    }
}