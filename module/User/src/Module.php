<?php
/**
 * Created by PhpStorm.
 * User: Cleber Carvalho
 * Date: 05/07/2017
 * Time: 21:34
 */

namespace User;


use User\Controller\Factory\AuthControllerFactory;
use User\Controller\AuthController;
use User\Service\Factory\AutheticationServiceFactory;
use Zend\Authentication\AuthenticationServiceInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;


class Module implements ConfigProviderInterface, ServiceProviderInterface, ControllerProviderInterface
{
    public function getConfig(){
        return include __DIR__ ."/../config/module.config.php";
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                AuthenticationServiceInterface::class => AutheticationServiceFactory::class
            ]
        ];
    }

    public function getControllerConfig()
    {
        return[
            'factories'=>[
                AuthController::class => AuthControllerFactory::class
            ]
        ];
    }
}

//Pegar adaptador de banco de dados
//Configurar um adpatador para adminsitrar a autenticação do usuário
//criar a sessão para guarda o usuário
//criar o serviço de AutheticationService