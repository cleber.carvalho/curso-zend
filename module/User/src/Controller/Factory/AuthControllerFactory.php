<?php
/**
 * Created by PhpStorm.
 * User: Cleber Carvalho
 * Date: 26/06/2017
 * Time: 21:15
 */

namespace User\Controller\Factory;


use Interop\Container\ContainerInterface;
use User\Controller\AuthController;
use Zend\Authentication\AuthenticationServiceInterface;

class AuthControllerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $authService = $container->get(AuthenticationServiceInterface::class);
        return new AuthController($authService);
    }
}