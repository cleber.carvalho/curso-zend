<?php
/**
 * Created by PhpStorm.
 * User: Cleber Carvalho
 * Date: 26/06/2017
 * Time: 21:15
 */

namespace User\Service\Factory;



use Interop\Container\ContainerInterface;
use Zend\Authentication\Adapter\DbTable\CallbackCheckAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\Db\Adapter\AdapterInterface;


class AutheticationServiceFactory
{

    //Pegar adaptador de banco de dados
    //Configurar um adpatador para adminsitrar a autenticação do usuário
    //criar a sessão para guarda o usuário
    //criar o serviço de AutheticationService


    public function __invoke(ContainerInterface $container)
    {
        $passwordCallbackVerify = function ($passwordInDatabase, $passwordSent){ /*PRIMEIRO ARG SENHA DO BANCO SEGUNDO A QUE FOI DIGITAGA*/
            return password_verify($passwordSent,$passwordInDatabase);
        };
        $dbAdapter = $container->get(AdapterInterface::class);
        $authAdapter = new CallbackCheckAdapter($dbAdapter,'users','username','password', $passwordCallbackVerify);
        $storage = new Session();
        return new AuthenticationService($storage,$authAdapter);
    }
}